use crate::account_widget::AccountWidget;
use gio::{Menu, MenuItem};
use glib::IsA;
use gtk4::{prelude::*, PopoverMenu, Widget};
use news_flash::models::VectorIcon;

#[derive(Clone, Debug)]
pub struct AccountPopover {
    pub widget: PopoverMenu,
    account_widget: AccountWidget,
}

impl AccountPopover {
    pub fn new<W: IsA<Widget>>(parent: &W) -> Self {
        let widget_item = MenuItem::new(Some("account_widget"), None);
        widget_item.set_attribute_value("custom", Some(&"account-box".to_variant()));

        let model = Menu::new();
        model.append_item(&widget_item);
        model.append(Some("Reset Account"), Some("win.reset-account"));
        model.append(Some("Update Login"), Some("win.update-login"));

        let popover = PopoverMenu::from_model(Some(&model));
        let account_widget = AccountWidget::new();

        popover.set_parent(parent);
        popover.add_child(&account_widget, "account-box");

        AccountPopover {
            widget: popover,
            account_widget,
        }
    }

    pub fn set_account(&self, vector_icon: Option<VectorIcon>, user_name: &str) {
        self.account_widget.set_account(vector_icon, user_name);
    }
}
