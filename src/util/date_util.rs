use crate::app::App;
use crate::i18n::i18n;
use crate::util::ClockFormat;
use chrono::{Duration, Local, NaiveDateTime, TimeZone};

pub struct DateUtil;

impl DateUtil {
    pub fn format(naive_utc: &NaiveDateTime) -> String {
        let local_datetime = Local.from_utc_datetime(naive_utc);
        let now = Local::now().naive_local();
        let now_date = now.date();
        let naive_local_date = local_datetime.naive_local().date();
        let clock_format = App::default().desktop_settings().clock_format();

        let time = if clock_format == ClockFormat::F12H {
            format!("{}", local_datetime.format("%I:%M %p"))
        } else {
            format!("{}", local_datetime.format("%k:%M"))
        };

        let date = if now_date == naive_local_date {
            i18n("Today")
        } else if now_date - naive_local_date == Duration::days(1) {
            i18n("Yesterday")
        } else {
            format!("{}", local_datetime.format("%e.%m.%Y"))
        };

        format!("{} {}", date, time)
    }
}
