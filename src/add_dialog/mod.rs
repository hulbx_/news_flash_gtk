mod add_category_widget;
mod add_feed_widget;
mod add_tag_widget;

use self::add_category_widget::AddCategoryWidget;
use self::add_feed_widget::AddFeedWidget;
use self::add_tag_widget::AddTagWidget;

use crate::app::App;
use glib::{clone, subclass};
use gtk4::{
    prelude::*, subclass::prelude::*, CallbackAction, CompositeTemplate, ListBox, Shortcut, Stack, Widget, Window,
};
use libadwaita::{subclass::prelude::AdwWindowImpl, traits::ActionRowExt, ActionRow, Window as AdwWindow};
use news_flash::models::{PluginCapabilities, Url};

mod imp {
    use super::*;

    #[derive(Debug, Default, CompositeTemplate)]
    #[template(resource = "/com/gitlab/newsflash/ui_templates/add_dialog.ui")]
    pub struct AddDialog {
        #[template_child]
        pub main_stack: TemplateChild<Stack>,
        #[template_child]
        pub select_list_box: TemplateChild<ListBox>,
        #[template_child]
        pub feed_row: TemplateChild<ActionRow>,
        #[template_child]
        pub category_row: TemplateChild<ActionRow>,
        #[template_child]
        pub tag_row: TemplateChild<ActionRow>,
        #[template_child]
        pub close_shortcut: TemplateChild<Shortcut>,

        #[template_child]
        pub add_tag_widget: TemplateChild<AddTagWidget>,
        #[template_child]
        pub add_category_widget: TemplateChild<AddCategoryWidget>,
        #[template_child]
        pub add_feed_widget: TemplateChild<AddFeedWidget>,
    }

    #[glib::object_subclass]
    impl ObjectSubclass for AddDialog {
        const NAME: &'static str = "AddDialog";
        type ParentType = AdwWindow;
        type Type = super::AddDialog;

        fn class_init(klass: &mut Self::Class) {
            klass.bind_template();
        }

        fn instance_init(obj: &subclass::InitializingObject<Self>) {
            obj.init_template();
        }
    }

    impl ObjectImpl for AddDialog {}

    impl WidgetImpl for AddDialog {}

    impl WindowImpl for AddDialog {}

    impl AdwWindowImpl for AddDialog {}
}

glib::wrapper! {
    pub struct AddDialog(ObjectSubclass<imp::AddDialog>)
        @extends Widget, gtk4::Window, AdwWindow;
}

impl AddDialog {
    pub fn new_for_feed_url<W: IsA<Window> + GtkWindowExt>(parent: &W, feed_url: &Url) -> Self {
        let dialog = Self::new(parent);
        let imp = dialog.imp();
        imp.main_stack.set_visible_child_name("feed_page");
        imp.add_feed_widget.parse_feed_url(feed_url);
        dialog
    }

    pub fn new<W: IsA<Window> + GtkWindowExt>(parent: &W) -> Self {
        let dialog = glib::Object::new::<Self>();
        dialog.set_transient_for(Some(parent));

        let imp = dialog.imp();
        let close_action = CallbackAction::new(|widget, _| {
            if let Ok(dialog) = widget.clone().downcast::<AddDialog>() {
                dialog.close();
            }
            true
        });
        imp.close_shortcut.set_action(Some(close_action));

        imp.add_tag_widget.connect_local(
            "tag-added",
            false,
            clone!(@weak dialog => @default-panic, move |_| {
                dialog.close();
                None
            }),
        );

        imp.add_category_widget.connect_local(
            "category-added",
            false,
            clone!(@weak dialog => @default-panic, move |_| {
                dialog.close();
                None
            }),
        );

        imp.add_feed_widget.connect_local(
            "feed-added",
            false,
            clone!(@weak dialog => @default-panic, move |_| {
                dialog.close();
                None
            }),
        );

        let features = App::default().features();
        imp.feed_row
            .set_sensitive(features.contains(PluginCapabilities::ADD_REMOVE_FEEDS));
        imp.category_row
            .set_sensitive(features.contains(PluginCapabilities::MODIFY_CATEGORIES));
        imp.tag_row
            .set_sensitive(features.contains(PluginCapabilities::SUPPORT_TAGS));

        imp.feed_row
            .connect_activated(clone!(@weak dialog => @default-panic, move |_row| {
                dialog.imp().main_stack.set_visible_child_name("feed_page");
            }));
        imp.category_row
            .connect_activated(clone!(@weak dialog => @default-panic, move |_row| {
                dialog.imp().main_stack.set_visible_child_name("category_page");
            }));
        imp.tag_row
            .connect_activated(clone!(@weak dialog => @default-panic, move |_row| {
                dialog.imp().main_stack.set_visible_child_name("tag_page");
            }));

        imp.main_stack.set_visible_child_name("start");

        dialog
    }
}
