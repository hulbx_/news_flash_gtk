use crate::app::App;
use crate::edit_feed_dialog::CategorySelectGObject;
use crate::i18n::i18n;
use gio::ListStore;
use glib::subclass;
use glib::{clone, subclass::*};
use gtk4::{prelude::*, subclass::prelude::*, Box, Button, CompositeTemplate, Image, Label, Stack, Widget};
use gtk4::{CheckButton, PropertyExpression};
use libadwaita::traits::{ActionRowExt, ComboRowExt, EntryRowExt, PreferencesGroupExt, PreferencesRowExt};
use libadwaita::{ActionRow, ComboRow, EntryRow, PreferencesGroup};
use news_flash::ParsedUrl;
use once_cell::sync::Lazy;
use std::cell::RefCell;
use std::rc::Rc;

use news_flash::models::{Category, CategoryID, Feed, FeedID, Url, NEWSFLASH_TOPLEVEL};

mod imp {
    use super::*;

    #[derive(Debug, CompositeTemplate)]
    #[template(resource = "/com/gitlab/newsflash/ui_templates/add_feed_widget.ui")]
    pub struct AddFeedWidget {
        #[template_child]
        pub stack: TemplateChild<Stack>,
        #[template_child]
        pub url_entry: TemplateChild<EntryRow>,
        #[template_child]
        pub parse_button: TemplateChild<Button>,
        #[template_child]
        pub error_message: TemplateChild<Label>,
        #[template_child]
        pub try_again_button: TemplateChild<Button>,
        #[template_child]
        pub feed_list: TemplateChild<PreferencesGroup>,
        #[template_child]
        pub select_button: TemplateChild<Button>,
        #[template_child]
        pub select_button_stack: TemplateChild<Stack>,
        #[template_child]
        pub feed_title_entry: TemplateChild<EntryRow>,
        #[template_child]
        pub favicon_image: TemplateChild<Image>,
        #[template_child]
        pub add_button: TemplateChild<Button>,
        #[template_child]
        pub category_combo: TemplateChild<ComboRow>,

        pub feed_url: Rc<RefCell<Option<Url>>>,
        pub feed_category: RefCell<Option<CategoryID>>,
    }

    impl Default for AddFeedWidget {
        fn default() -> Self {
            AddFeedWidget {
                stack: TemplateChild::default(),
                url_entry: TemplateChild::default(),
                parse_button: TemplateChild::default(),
                feed_list: TemplateChild::default(),
                error_message: TemplateChild::default(),
                try_again_button: TemplateChild::default(),
                select_button: TemplateChild::default(),
                select_button_stack: TemplateChild::default(),
                feed_title_entry: TemplateChild::default(),
                favicon_image: TemplateChild::default(),
                add_button: TemplateChild::default(),
                category_combo: TemplateChild::default(),

                feed_url: Rc::new(RefCell::new(None)),
                feed_category: RefCell::new(None),
            }
        }
    }

    #[glib::object_subclass]
    impl ObjectSubclass for AddFeedWidget {
        const NAME: &'static str = "AddFeedWidget";
        type ParentType = Box;
        type Type = super::AddFeedWidget;

        fn class_init(klass: &mut Self::Class) {
            klass.bind_template();
        }

        fn instance_init(obj: &subclass::InitializingObject<Self>) {
            obj.init_template();
        }
    }

    impl ObjectImpl for AddFeedWidget {
        fn signals() -> &'static [Signal] {
            static SIGNALS: Lazy<Vec<Signal>> = Lazy::new(|| vec![Signal::builder("feed-added").build()]);
            SIGNALS.as_ref()
        }

        fn constructed(&self) {
            self.obj().init();
        }
    }

    impl WidgetImpl for AddFeedWidget {}

    impl BoxImpl for AddFeedWidget {}
}

glib::wrapper! {
    pub struct AddFeedWidget(ObjectSubclass<imp::AddFeedWidget>)
        @extends Widget, Box;
}

impl Default for AddFeedWidget {
    fn default() -> Self {
        glib::Object::new::<Self>()
    }
}

impl AddFeedWidget {
    pub fn new() -> Self {
        Self::default()
    }

    fn init(&self) {
        let imp = self.imp();

        self.setup_category_selector();

        // make parse button sensitive if entry contains text and vice versa
        let parse_button = imp.parse_button.get();
        imp.url_entry
            .connect_changed(clone!(@weak parse_button => @default-panic, move |entry| {
                if entry.text().as_str().is_empty() {
                    parse_button.set_sensitive(false);
                } else {
                    parse_button.set_sensitive(true);
                }
            }));

        // hit enter in entry to parse url
        imp.url_entry
            .connect_entry_activated(clone!(@weak parse_button => @default-panic, move |_entry| {
                if parse_button.get_sensitive() {
                    parse_button.emit_clicked();
                }
            }));

        // parse url and switch to feed selection or final page
        imp.parse_button.connect_clicked(clone!(
            @weak self as widget => @default-panic, move |_button|
        {
            let imp = widget.imp();

            let mut url_text = imp.url_entry.text().as_str().to_owned();
            if !url_text.starts_with("http://") && !url_text.starts_with("https://") {
                url_text.insert_str(0, "https://");
            }
            if let Ok(url) = Url::parse(&url_text) {
                widget.parse_feed_url(&url);
            } else {
                log::error!("No valid url: '{}'", url_text);
                imp.stack.set_visible_child_name("error");
                imp.error_message.set_label(&i18n("Not a valid URL"));
            }
        }));

        imp.try_again_button.connect_clicked(clone!(
            @weak self as widget => @default-panic, move |_button|
        {
            widget.imp().stack.set_visible_child_name("url_page");
        }));

        let add_button = imp.add_button.get();
        imp.feed_title_entry
            .connect_changed(clone!(@weak add_button => @default-panic, move |entry| {
                add_button.set_sensitive(!entry.text().as_str().is_empty());
            }));

        imp.add_button.connect_clicked(clone!(
            @weak self as widget => @default-panic, move |_button|
        {
            let imp = widget.imp();

            let feed_url = match imp.feed_url.borrow().clone() {
                Some(url) => url,
                None => {
                    log::error!("Failed to add feed: No valid url");
                    App::default().in_app_notifiaction("Failed to add feed: No valid url");
                    return;
                }
            };
            let feed_title = if imp.feed_title_entry.text().as_str().is_empty() { None } else { Some(imp.feed_title_entry.text().as_str().into()) };

            App::default().add_feed(feed_url, feed_title, imp.feed_category.borrow().clone());
            widget.emit_by_name::<()>("feed-added", &[]);
        }));
    }

    fn setup_category_selector(&self) {
        App::default().execute_with_callback(
            |news_flash, _client| async move {
                if let Some(news_flash) = news_flash.read().await.as_ref() {
                    if let Ok((categories, _mappings)) = news_flash.get_categories() {
                        return categories;
                    }
                }

                Vec::new()
            },
            clone!(@weak self as this => @default-panic, move |_app, categories: Vec<Category>| {
                let imp = this.imp();

                let list_store = ListStore::new(CategorySelectGObject::static_type());
                list_store.append(&CategorySelectGObject::new(&NEWSFLASH_TOPLEVEL, "None"));
                for category in &categories {
                    list_store.append(&CategorySelectGObject::from_category(category));
                }

                let expression = PropertyExpression::new(
                    CategorySelectGObject::static_type(),
                    None::<&PropertyExpression>,
                    "label",
                );
                imp.category_combo.set_expression(Some(&expression));
                imp.category_combo.set_model(Some(&list_store));
                imp.category_combo.set_selected(0);

                imp.category_combo
                    .connect_selected_item_notify(clone!(@weak this => @default-panic, move |combo| {
                        if let Some(selected) = combo
                            .selected_item()
                            .and_then(|obj| obj.downcast::<CategorySelectGObject>().ok())
                        {
                            let imp = this.imp();
                            let mut category_id = None;
                            if *selected.id() != *NEWSFLASH_TOPLEVEL {
                                category_id = Some(selected.id().clone());
                            }
                            imp.feed_category.replace(category_id);
                        }
                    }));
            }),
        );
    }

    fn fill_mupliple_feed_list(&self, feed_vec: Vec<Feed>) {
        let imp = self.imp();
        let selected_feed_url: Rc<RefCell<Option<Url>>> = Rc::new(RefCell::new(None));

        imp.select_button.connect_clicked(
            clone!(@weak self as widget, @strong selected_feed_url => @default-panic, move |button|
            {
                let imp = widget.imp();

                if let Some(feed_url) = selected_feed_url.take() {
                    imp.select_button_stack.set_visible_child_name("spinner");
                    button.set_sensitive(false);

                    let feed_id = FeedID::new(feed_url.as_str());

                    App::default().execute_with_callback(
                        |_news_flash, client| async move {
                            news_flash::feed_parser::download_and_parse_feed(&feed_url, &feed_id, None, &client).await
                        },
                        clone!(@weak widget => @default-panic, move |_app, res| {
                            let imp = widget.imp();

                            if let Ok(ParsedUrl::SingleFeed(feed)) = res {
                                widget.fill_feed_page(*feed);
                                imp.stack.set_visible_child_name("parsed_feed_page");
                            } else {
                                imp.stack.set_visible_child_name("error");
                                imp.error_message.set_label(&i18n("Can't parse Feed"))
                            }

                            imp.select_button_stack.set_visible_child_name("text");
                            imp.select_button.set_sensitive(true);
                        })
                    );
                }
            }),
        );
        let mut group: Option<CheckButton> = None;
        for feed in feed_vec {
            let radio_button = CheckButton::new();
            radio_button.set_group(group.as_ref());

            let select_button = imp.select_button.get();
            radio_button.connect_toggled(clone!(@strong feed.feed_url as feed_url, @strong selected_feed_url => @default-panic, move |radio_button| {
                if radio_button.is_active() {
                    selected_feed_url.replace(feed_url.clone());
                }
                select_button.set_sensitive(true);
            }));

            let row = ActionRow::new();
            row.set_title(&feed.label);
            row.set_activatable(true);
            row.set_activatable_widget(Some(&radio_button));
            row.add_prefix(&radio_button);
            if let Some(url) = feed.feed_url {
                row.set_widget_name(url.as_str());
            }

            imp.feed_list.add(&row);

            group = Some(radio_button);
        }
    }

    fn fill_feed_page(&self, feed: Feed) {
        let imp = self.imp();

        imp.feed_title_entry.set_text(&feed.label);
        if let Some(new_feed_url) = &feed.feed_url {
            imp.feed_url.replace(Some(new_feed_url.clone()));
        } else {
            imp.feed_url.take();
        }

        let favicon_image = imp.favicon_image.get();
        let feed_clone = feed;
        App::default().execute_with_callback(
            |_news_flash, client| async move {
                news_flash::util::favicon_cache::FavIconCache::fetch_new_icon(
                    None,
                    &feed_clone,
                    &client,
                    None,
                    Some(128), // image is 64x64 so try to fetch a 128px icon because of 2x scaling
                )
                .await
            },
            move |_app, favicon| {
                if let Some(data) = favicon.data {
                    let bytes = glib::Bytes::from_owned(data);
                    if let Ok(texture) = gdk4::Texture::from_bytes(&bytes) {
                        favicon_image.set_from_paintable(Some(&texture));
                    }
                }
            },
        );
    }

    pub fn parse_feed_url(&self, url: &Url) {
        let imp = self.imp();

        self.setup_category_selector();

        // set 'next' button insensitive and show spinner
        imp.stack.set_visible_child_name("spinner");
        imp.parse_button.set_sensitive(false);

        let feed_id = FeedID::new(url.as_str());
        let url_clone = url.clone();

        App::default().execute_with_callback(
            |_news_flash, client| async move {
                news_flash::feed_parser::download_and_parse_feed(&url_clone, &feed_id, None, &client).await
            },
            clone!(@weak self as widget, @strong url => @default-panic, move |_app, res| {
                let imp = widget.imp();

                // parse url
                match res {
                    Ok(result) => match result {
                        ParsedUrl::MultipleFeeds(feed_vec) => {
                            // url has multiple feeds: show selection page and list them there
                            imp.stack.set_visible_child_name("multiple_selection_page");
                            widget.fill_mupliple_feed_list(feed_vec);
                        }
                        ParsedUrl::SingleFeed(feed) => {
                            // url has single feed: move to feed page
                            imp.stack.set_visible_child_name("parsed_feed_page");
                            widget.fill_feed_page(*feed);
                        }
                    },
                    Err(error) => {
                        log::error!("No feed found for url '{}': {}", url, error);
                        imp.stack.set_visible_child_name("url_page");
                        imp.url_entry.set_text(&url.to_string());
                        imp.stack.set_visible_child_name("error");
                        imp.error_message.set_label(&i18n("No Feed found"))
                    }
                }

                // set 'next' buton sensitive again
                imp.parse_button.set_sensitive(true);
            }),
        );
    }
}
